module AsciiMath (readAscii, writeTeX, compile, run, AsciimathException(..), renderError, printAndExit) where

import Control.Exception (throw)
import Data.Text (Text)

import Ast                           (Code)
import Exception                     (AsciimathException(ErrorAndSource,IOError)
                                     ,printAndExit,renderError)
import Lexer                         (get_tokens)
import Parser                        (parseAscii)
import TeXWriter                     (writeTeX)

readAscii :: Text -> Either AsciimathException Code
readAscii t = parseAscii =<< get_tokens t

compile :: Text -> Either AsciimathException Text
compile t = fmap writeTeX $ readAscii t

run :: Text -> Text
run t = case compile t of
  Right txt -> txt
  Left e -> throw $ ErrorAndSource e t
