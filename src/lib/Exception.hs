{-# LANGUAGE DeriveDataTypeable,OverloadedStrings #-}
module Exception (AsciimathException(..), Position(..), ErrorType(..), printAndExit, renderError) where

import System.Exit (exitWith, ExitCode(ExitFailure))
import System.IO (hFlush, stderr)

import Data.Text.IO (hPutStrLn)
import Data.Text (Text,pack)

import Control.Exception (Exception)
import Data.Data

showT :: (Show a) => a -> Text
showT = pack . show

-- Location
-- abs, line, column, length
data Position
  = Position !Int !Int !Int
  | PositionElement !Int !Int !Int !Int
  deriving (Data, Show, Eq)

data ErrorType = Parser | Lexical deriving (Show)

data AsciimathException
  = AsciiError ErrorType Text Position
  | ErrorAndSource AsciimathException Text
  | IOError Text
  deriving (Show, Typeable)

instance Exception AsciimathException

renderError :: AsciimathException -> Text
renderError (AsciiError t msg (PositionElement _ l c len)) =
  "Line " <> showT l <>
  ", characters " <> showT c <> "-" <> showT (c + len) <> ":\n" <>
  showT t <> " error near: \"" <> msg <> "\""
renderError (AsciiError t msg (Position _ l c)) =
  "Line " <> showT l <>
  ", characters " <> showT c <> "-" <> showT (c + 1) <> ":\n" <>
  showT t <> " error near: " <> msg
renderError (ErrorAndSource exn src) =
  "In expression:\n" <> src <> "\n" <>
  renderError exn
renderError (IOError text) = text <> "\n"

printAndExit :: AsciimathException -> IO ()
printAndExit e = do
  hPutStrLn stderr $ renderError e
  hFlush stderr
  exitWith (ExitFailure 1)
