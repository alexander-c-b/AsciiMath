{-# LANGUAGE OverloadedStrings,LambdaCase #-}
module Main (main) where

import qualified Control.Exception as E
import Text.Pandoc.Definition (Format(..),Inline(Math,RawInline),MathType(..))
import Text.Pandoc.JSON (toJSONFilter)
import Text.TeXMath (writeMathML,readTeX,DisplayType(..))
import Text.XML.Light.Output (showElement)
import Data.Text (Text,pack)
import AsciiMath (run,printAndExit,AsciimathException(IOError))


main :: IO ()
main = E.catch (toJSONFilter asciimath) printAndExit

asciimath :: Maybe Format -> Inline -> Inline
asciimath f (Math t s)
  | f == Just (Format "context") = toConTeXt t (run s)
  | otherwise = Math t (run s)
asciimath _ x = x

toConTeXt :: MathType -> Text -> Inline
toConTeXt t s = case readTeX s of
  Left msg          -> E.throw $ IOError msg
  Right expressions -> RawInline (Format "context") $
    (\text -> "\\xmlprocessdata{}{" <> text <>"}{}") $
    pack $ showElement $ writeMathML (mathType t) expressions

mathType :: MathType -> DisplayType
mathType = \case {InlineMath -> DisplayInline; DisplayMath -> DisplayBlock}
