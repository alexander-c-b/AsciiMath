{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Control.Exception as E
import qualified Data.Text.IO as TIO
import AsciiMath

main :: IO ()
main = E.catch (TIO.interact $ (<> "\n") . run) printAndExit
